#!/usr/bin/env python3

import cgi
import http.cookies
import html
import sys
import codecs
import re
import psycopg2
import datetime
from psycopg2 import sql

# Кодировка utf-8

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

'''Обработка формы'''
f = False
form = cgi.FieldStorage()
text1 = form.getvalue("field-name-1", "")
cookie = {'name': text1}
text1 = html.escape(text1)
text2 = form.getvalue("field-email", "")
cookie['email'] = text2
text2 = html.escape(text2)
date1 = datetime.datetime.strptime(form.getvalue("field-date"), "%Y-%m-%d")
date1 = date1.date()
cookie['date'] = date1.strftime("%Y-%m-%d")
sex = form.getvalue('radio-group-1')
cookie['sex'] = sex
count = int(form.getvalue('radio-group-2'))
cookie['count'] = str(count)
subject = form.getvalue('field-name-4')
cookie['subject'] = subject
bio = form.getvalue('field-name-2')
if not bio is None:
    bio = html.escape(bio)
    cookie['bio'] = bio
else:
    cookie['bio'] = ''
if form.getvalue('check-1'):
    checkbox_flag = True
    cookie['check'] = 'on'
else:
    checkbox_flag = False
    cookie['check'] = 'off'

print('Set-cookie: field-name-1 = %s', cookie['name'])
print('Set-cookie: field-name-email = %s', cookie['email'])
print('Set-cookie: field-date = %s', cookie['date'])
print('Set-cookie: radio-group-1 = %s', cookie['sex'])
print('Set-cookie: radio-group-2 = %s', cookie['count'])
print('Set-cookie: field-name-4 = %s', cookie['subject'])
print('Set-cookie: field-name-2 = %s', cookie['bio'])
print('Set-cookie: check-1 = %s', cookie['check'])

print("Content-type: text/html")
print()
print('''<DOCTYPE html>
<html lang="ru">
<head>
<title>backend-4</title>
<meta charset="UTF-8">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>
<body>
<form method="POST" action="/cgi-bin/form.py">
''')
if text1 == '':
    print('<label class="bg-danger">Имя:<br><input name="field-name-1" value="'+cookie['name']+'"></label><br>')
    print('<p>Это поле обязательно для заполнения и в нём нельзя использовать символы &lt;, &lt;, &quot;</p>')
    f = True
else:
    print('<label>Имя:<br><input name="field-name-1" value="'+cookie['name']+'"></label><br>')
if text2 == '':
    print('<label class="bg-danger">Текстовое поле email:<br><input name="field-email" type="email" value="'+cookie['email']+'"></label><br>')
    print('<p>Это поле обязательно для заполнения и в нём нельзя использовать символы &lt;, &lt;, &quot;</p>')
    f = True
else:
    print('<label>Текстовое поле email:<br><input name="field-email" type="email" value="'+cookie['email']+'"></label><br>')
print('<label>Текстовое поле даты:<br><input name="field-date" value="'+cookie['date']+'" type="date"></label><br>')
if sex == 'Мужской':
    print('''<label><input type="radio" checked="checked" name="radio-group-1" value="Мужской"/>Мужской</label>
            <label><input type="radio" name="radio-group-1" value="Женский"/>Женский</label><br>
            Количество конечностей:<br>
       ''')
else:
    print('''<label><input type="radio" name="radio-group-1" value="Мужской"/>Мужской</label>
        <label><input type="radio" checked="checked" name="radio-group-1" value="Женский"/>Женский</label><br>
        Количество конечностей:<br>
       ''')
for i in range(1, 9):
    if count == i:
        print('<label><input type="radio" checked="checked" name="radio-group-2" value="'+str(i)+'"/>'+str(i)+'</label>')
    else:
        print('<label><input type="radio" name="radio-group-2" value="'+str(i)+'"/>'+str(i)+'</label>')
print('<br><label>Сверхспособности:<br><select name="field-name-4" multiple="multiple">')
strsubject = ' '.join(subject)
if re.search("Бессмертие", strsubject):
    print('<option value="Бессмертие" selected="selected">Бессмертие</option>')
else:
    print('<option value="Бессмертие">Бессмертие</option>')
if re.search("Прохождение через стены", strsubject):
    print('<option value="Прохождение через стены" selected="selected">Прохождение через стены</option>')
else:
    print('<option value="Прохождение через стены">Прохождение через стены</option>')
if re.search("Левитация", strsubject):
    print('<option value="Левитация" selected="selected">Левитация</option>')
else:
    print('<option value="Левитация">Левитация</option>')
print('''</select>
        </label><br>''')

if bio:
    print('<label>Биография:<br><textarea name="field-name-2" value="'+cookie['bio']+'"></textarea></label><br>')
else:
    print('<label class="bg-danger">Биография:<br><textarea name="field-name-2" value="'+cookie['bio']+'"></textarea></label><br>')
    print('<p>Это поле обязательно для заполнения и в нём нельзя использовать символы &lt;, &lt;, &quot;</p>')
    f = True

if checkbox_flag:
    print('''
       Чекбокс:<br /><label><input type="checkbox" name="check-1" checked="checked">C контрактом ознакомлен</label><br>
        <input type="submit" class="btn-primary" value="Отправить">
        </form>
       ''')
else:
    print('''Чекбокс:<br /><label class="bg-danger"><input type="checkbox" name="check-1">C контрактом ознакомлен</label><br>
        <p>Необходимо согласить с условиями контракта для отправки формы</p>
        <input type="submit" class="btn-primary" value="Отправить">
        </form>
   ''')
    f = True

if f:
    print('''<p>Пожалуйста, исправьте неверно заполненные поля и отправьте форму заново</p>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
        </body>
        </html>
       ''')
    exit()
else:
    print('''<p>Форма успешно отправлена</p>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
            </body>
            </html>
           ''')

'''Тут работа с БД'''
conn = psycopg2.connect(
    dbname='postgres',
    user='myuser',
    password='UigfjL0f',
    host='localhost'
)
with conn.cursor() as cursor:
    conn.autocommit = True
    values = [
        (0, text1, text2, date1, sex, count, subject, bio, checkbox_flag),
    ]
    insert = sql.SQL(
        'INSERT INTO form (id_person, first_name, email, nowdate, sex, kolvo, abilities, bio, checkbox) VALUES {}').format(
        sql.SQL(',').join(map(sql.Literal, values))
    )
    cursor.execute(insert)
conn.close()
